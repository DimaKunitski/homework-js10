const pass = document.querySelector('#pass');
const cfPass =document.querySelector('#cf-pass');
const pAlert = document.querySelector('form p');
const btnSubmit = document.querySelector('button');
const btnShow = document.querySelectorAll('label i');

btnSubmit.addEventListener('click', (e) =>{
    e.preventDefault();
    let txtErr = "Нужно ввести одинаковые значения";
    let txtSuccess = "You are welcome";
    let result = (pass.value === cfPass.value);
    pAlert.textContent = result ? txtSuccess : txtErr;
});

btnShow.forEach(item =>{
    let show = false;
    item.addEventListener('click', ()=>{
        show = !show;
        item.parentElement.children[0].setAttribute(
            'type',
            show ? 'text' : 'password'
        );
        item.parentElement.children[1].setAttribute(
            'class',
            show ? 'fa fa-eye-slash' : 'fa fa-eye'
        );
    });
})